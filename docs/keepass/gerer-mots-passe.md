---
lang: fr
...

# Gérer les mots de passe avec Keepass

## Pour commencer : installer Keepass

* Télécharger [Keepass](https://keepass.info/download.html)

## Ouvrir le fichier dans Keepass

Le fichier des mots de passe Alternatiba est dans le dossier `TECH/test_keepass`.

* Cliquer sur **Ouvrir > Ouvrir un fichier**

* Sélectionner le fichier de mot de passe dans le dossier TECH
    
    ![Sélectionner le fichier](images/keepass_open_file.png)

* Entrez le mot de passe principal
    
    ![Entrez mot de passe](images/keepass_enterkey.png)

Vous y êtes

![Keepass](images/keepass_alternatiba.png)

## Consulter un mot de passe

Lorsque le fichier des mots de passe est dévérouillé, vous pouvez l'utiliser sans remettre le mot de passe principal.

* Recherchez le compte dans la barre de recherche

* Copier le mot de passe dans le presse-papier

## Ajouter un mot de passe

* Allez dans **Édition > Ajouter une entrée**

* Saisissez les informations du compte et mot de passe à conserver. 

    ![Saisir mot de passe](images/keepass_addpassword.png)

    Si vous venez de créer le compte, il est conseillé d'utiliser le mot de passe généré, plus sécurisé.
