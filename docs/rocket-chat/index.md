# Discuter en ligne avec Rocket.Chat

Rocket.Chat est un outil de discussion instantanée, basé sur un logiciel libre.

## Objectifs

À Alternatiba et ANV, il permet de :

* Échange sur des sujets spécifiques
* Communication Team / groupes Alternatiba / ANV-COP21 / militante⋅e⋅s
* Suivre les actualités du réseau
* Réseau social interne, intéraction entre les membres
* Accueillir des nouveaux⋅elles

## Fonctionnalités

<!--v-->

### Discussion en ligne …

Envoyer des messages

-   sur les salons publics,
-   dans des groupes privés
-   de personne à personne

<!--v-->

### … et sur mobile

-   Applications mobiles pour Android et Iphone

<!--v-->

### Fonctionnalités pratiques

-   Notifications par mail hors connexion
-   Partage de fichiers et d'images
-   Recherche dans l'historique des messages
-   Création et modération des canaux de discussion

<!--v-->

### Liens avec des flux extérieurs

 Rocket.chat peut être connecté avec d'autres applications pour envoyer ou recevoir des informations (Webhooks, Zapier, …)

<!--v-->

## Logiciel libre, hébergement alternatif

Rocket.Chat est un logiciel libre, hébergé par IndieHosters, membre du collectif CHATONS (Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires)

* respect de notre vie privée
* dans les valeurs de Alternatiba

## Les tutoriels

- [Utilisation de Rocket.Chat](rocket-chat/rocket-chat-tuto.md)
- [Formater les messages dans Rocket.Chat](rocket-chat/rocket-chat-formatage-messages.md)
- [Administration de Rocket.Chat](rocket-chat/rocket-chat-admin-tuto.md)
