---
title: Outil de communication interne
author: Alternatiba
date: Avril 2018
theme: beige
separator: <!--s-->
verticalSeparator: <!--v-->
---

## Sommaire

<!--v-->

* Rappels objectifs
* Choix de l'outil : Rocket.Chat
* Fonctionnalités
* Configuration pour Alternatiba
* Prise en main

<!--s-->

## Rappel des objectifs

<!--v-->

* Échange sur des sujets spécifiques
* Communication Team / groupes Alternatiba / ANV-COP21 / militante⋅e⋅s
* Suivre les actualités du réseau
* Réseau social interne, intéraction entre les membres
* Accueillir des nouveaux⋅elles

<aside class="notes">

- permettre un échange entre les groupes Alternatiba/ANV-COP21/militant.e.s sur des sujets spécifiques
- faciliter la communication entre la Team et les groupes Alternatiba/ANV-COP21/militant.e.s
- permettre à tout notre réseau, y compris les personnes ne faisant pas partie d'un groupe Alternatiba/ANV-COP1 de suivre les actualités
- créer un réseau social interne où chaque membre du mouvement sait qu'il y retrouve sa communauté, peut accéder à toutes les informations dont il y a besoin et interagir avec les autres membres de la communauté
- recruter de nouveaux militant.e.s, bénévoles, activistes sur les différents projets et événements que nous organisons grâce à un canal de communication direct

</aside>

<!--s-->

## Fonctionnalités de Rocket.Chat

<!--v-->

### Discussion en ligne …

Envoyer des messages

-   sur les salons publics,
-   dans des groupes privés
-   de personne à personne

<!--v-->

### … et sur mobile

-   Applications mobiles pour Android et Iphone

<!--v-->

### fonctionnalités pratiques

-   Notifications par mail hors connexion
-   Partage de fichiers et d'images
-   Recherche dans l'historique des messages
-   Création et modération des canaux de discussion

<!--v-->

### Liens avec des flux extérieurs

 Rocket.chat peut être connecté avec d'autres applications pour envoyer ou recevoir des informations (Webhooks, Zapier, …)

<!--v-->

### Logiciel libre, hébergement alternatif

Rocket.Chat est un logiciel libre, hébergé par IndieHosters, membre du collectif CHATONS (Collectif des Hébergeurs Alternatifs Transparents Ouverts Neutres et Solidaires)

* respect de notre vie privée
* dans les valeurs de Alternatiba

<!--s-->

## Le chat d'Alternatiba

<!--v-->

### Les salons

* general: accueil et discussions générales
* fil-infos: actus des réseaux sociaux et du site

Où demander de l'aide ? **aide_rocketchat**

<!--v-->

### Salons thématiques

* alternatives_territoriales
* intervillages
* tour_alternatiba_2018

Et les salons **coin_militant** et **coin_detente**

<!--s-->

### On commence ?

[Prise en main](./rocket-chat-tuto.html)
