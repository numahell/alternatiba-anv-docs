---
title: Administration de Rocket.Chat
author: Alternatiba
date: Mai 2019
theme: beige
separator: <!--s-->
verticalSeparator: <!--v-->
---

## Sommaire

<!--v-->

- Interface d'administration
- Gestion des user
- Rôle et permissions
- Paramètres de l'instance

<!--s-->

## Interface d'administration

<!--v-->

### Accéder à l'administration

![](media/50-rocket-chat-admin-3.png)

<!--v-->

### Interface d'administration

Deux parties

- gestion des salons, comptes et contenus
- paramétrage d'administration de Rocket.Chat

<!--s-->

## Gestion des comptes

<!--v-->

- Chercher un utilisateur
- Modifier un utilisateur
- Valider manuellement son email
- Changer le mot de passe d'un utilisateur
- Changer le rôle d'un utilisateur (au niveau global)
- Supprimer un utilisateur

<!--v-->

### Rôles et permissions

Un rôle est un ensemble de permissions qui sont accordées à un⋅e utilisateur⋅ice. Le rôle par défaut est `user`.

<!--v-->

#### Portée des rôles

Les rôles peuvent s'appliquer soit :

- au niveau d'un canal, par un⋅e propriétaire du canal (`Rooms`)
- au niveau global, par un⋅e administrateur⋅ice (`Global`)

Par exemple : Bob peut être `moderator`, mais seulement au niveau d'un ou plusieurs canaux, et si Alice est `admin` ce sera au niveau global.

<!--v-->

#### Rôles usuels

- admin (Global) - Ont accès à tous les paramètres et outils d'administration.
- moderator (Rooms) - Ont les permissions pour modérer un canal. Sont choisis par la ou le propriétaire d'un canal.
- owner (Rooms) - Ont les droits de propriété d'un canal. Quand un utilisateur crée un canal, il est automatiquement propriétaire de celui-ci. Il peut y avoir plusieurs propriétaires d'un canal.
- user (Global) - Droits par défaut, reçus à l'inscription.
- bot (Global) - Rôle spécial pour les bots, avec des permissions liées aux fonctionnalités des bots.
- leader (Rooms) - Pas de permissions spécifiques, ils sont simplement affichés sur l'entête d'un canal.

Seuls les admin, owner et leader peuvent écrire sur un canal en lecture seule.

<!--v-->

### Changer le rôle d'un user

#### Au niveau global

Pour le rôle `admin`, deux façons

- Sélectionner un user, cliquer sur `…` puis sur "Promouvoir administateur" ou "Supprimer administateur"

![](media/50-rocket-chat-admin-edit-role.png)

<!--v-->

- **ou** : modifier l'user, et ajouter ou enlever un rôle

![](media/50-rocket-chat-admin-suprimer-admin.png)

<!--v-->

#### Au niveau d'un canal

Depuis le canal concerné, chercher l'user, puis cliquer sur `…` pour modifier ses rôles `owner` ou `moderator`

<!--s-->

## Paramètres de l'instance

<!--v-->

### Paramètres généraux

- Rechercher un paramètre

<!--v-->

### Autres paramètres

- Changer l'apparence
- Paramètres des comptes
- Paramètres des emails
- Paramètres des fichiers
- Paramètres des messages


<!--s-->

## Ressources

- [Manuel de l'administration](https://rocket.chat/docs/administrator-guides/) (en anglais, officiel)

<!--s-->

<a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/3.0/fr/88x31.png" /></a><br />Ce contenu est mis à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-sa/3.0/fr/">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 3.0 France</a>.
