---
title: Mettre en forme ses messages sur Rocket.Chat
author: Alternatiba
date: Août 2019
theme: beige
---

# Mettre en forme ses messages sur Rocket.Chat

Voilà quelques astuces pour mettre en forme vos messages : texte en gras ou en italique, insertion de liens ou de citations, rédaction de listes à puces, tout cela peut être utilisé simplement dans vos messages Rocket.Chat.

La mise en forme s'effectue en insérant des symboles dans le texte (`#`,`*`,`>`,etc.). Une fois le message rédigé, Rocket.Chat les transforme en indications de mise en forme.

Pour reproduire les différentes astuces regardez dans les cadres grisés quelles sont les combinaisons de symboles qui ont permis de mettre en forme le texte. Vous devez reproduire **exactement** ces combinaisons : ne mettez pas d'espaces s'il n'y en a pas dans l'exemple, sautez une ligne s'il le faut, etc. _**Bonne lecture**_ !

## Gras, italique, barré

Pour mettre certains éléments en valeur, on peut formater le texte avec *de l’italique* ou **du gras pour les choses à retenir**. On peut aussi ~~rayer les mentions inutiles~~ ou indiquer des `commandes informatiques`.

```
Pour mettre certains éléments en valeur, on peut formater le texte avec *de l’italique* ou **du gras pour les choses à retenir**, ~~rayer les mentions inutiles~~, ou indiquer des `commandes informatiques`.
```

## Gérer les paragraphes

Pour aller à la ligne, pensez à appuyer sur `MAJ+Entrée`, très pratique pour envoyer tout son texte en un seul morceau et éviter d’inonder tout le monde de notifications quand vous écrivez un long message ! Si jamais vous avez envoyez un message par erreur, n’hésitez pas à modifier le message publié : cliquez sur les trois points qui apparaissent en haut à droite du message publié, puis sur « Modifier » pour finir votre message. 
Autre façon de modifier son message (sur PC) : positionnez-vous dans la zone de rédaction des messages puis appuyez sur la touche “flèche du haut”.

## Intégrer des liens vers des site web

Pour améliorer la lisibilité de vos messages, ajoutez [des hyperliens](https://ieet.fr/pollueurs/) au lieu de copier-coller l’url au milieu de votre texte.

```
Pour améliorer la lisibilité de vos messages, ajoutez [des hyperliens](https://ieet.fr/pollueurs/) au lieu de copier-coller l’url au milieu de votre texte.
```

Un aperçu des page web est automatiquement généré en bas de votre message une fois celui-ci publié (si tout se passe bien, le logiciel est ~~parfois~~ souvent capricieux).

## Citer des textes

On est fréquemment amené·e à recopier des mails, des extraits d’articles de presse ou de textes divers. L’outil de **citation** permet de les mettre en forme facilement et de bien les distinguer du reste du texte :


> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue tortor odio, sed euismod odio dignissim vitae. In eleifend ex magna, eu hendrerit nisl commodo id. Aliquam hendrerit placerat gravida. Quisque pulvinar vitae sem et feugiat. Nam urna ex, fringilla eu tellus tristique, ullamcorper convallis leo.


Pour sortir de la citation et retourner au corps du texte, il est nécessaire de sauter une ligne (même principe pour sortir des listes à puces).


> Et on peut reprendre une nouvelle citation.


```
> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Proin congue tortor odio, sed euismod odio dignissim vitae. In eleifend ex magna, eu hendrerit nisl commodo id. Aliquam hendrerit placerat gravida. Quisque pulvinar vitae sem et feugiat. Nam urna ex, fringilla eu tellus tristique, ullamcorper convallis leo.


Pour sortir de la citation et retourner au corps du texte, il est nécessaire de sauter une ligne


> Et on peut reprendre une nouvelle citation.
```

## Utiliser des listes à puces

Pour les **listes à puces** c’est très simple et vous en avez peut-être déjà fait par hasard :

- faire les courses
- passer l’aspirateur
- repeindre les volets

Comme pour les citations, sautez une ligne avant et après la liste

```
- faire les courses
- passer l’aspirateur
- repeindre les volets

Comme pour les citations, sautez une ligne pour sortir de la liste
```

## Utiliser des séparateurs

La **ligne grise** est assez élégante pour faire office de séparateur entre deux paragraphes. 

---

Il suffit d’écrire `---` pour l’afficher.

Vous pouvez aussi laisser une ligne en blanc pour aérer votre texte s'il devient illisible !
&nbsp;
Nouveau paragraphe, ouf on respire !

```
Vous pouvez aussi laisser une ligne en blanc pour aérer votre texte s'il devient illisible !
&nbsp;
Nouveau paragraphe, ouf on respire !
```
