---
title: Personnaliser son site WordPress
summary: Widgets, menus, personnalisation du thème, etc.
author: JPeGfr, Numahell
license: CC0
date: Juillet 2020
---

Ce tutoriel s'adresse aux personnes qui utilisent Wordpress
pour l'édition, et qui souhaitent en savoir plus sur la personnalisation
du site.

Licence : CC-0

Au sommaire :

- Rubriques tableau de bord : ‘Apparence’, ‘Réglages’, ‘contacts’, ‘commentaires’
- Modifications / créations de menus
- Modification des pieds-de-page, en-têtes, colonnes latérales
- Fonctionnement des thèmes

## Les rubriques du tableau de bord

### Menus pour l'édition

- Articles
- Media
- Pages
- Commentaires
- … autres types d'objets disponibles grâce aux plugins

### Menus pour la personnalisation

- Apparence
    - Thèmes → choisir un thème
    - Personnaliser → le personnaliser
    - Widgets → placer des blocs dans le pied de page ou la barre latérale
    - Menus → gérer les menus
    - Arrière-plan → modifier l'arrière-plan
    - Éditeur de thème → édition avancée du thème (attention zone dangereuse)
- Extensions
- Utilisateurs
- Outils
- Réglages
    - réglages généraux du site
    - réglages de certains plugins
- Panneaux spécifiques à des plugins

## Gestion des menus

Par défaut, le menu principal est constitué des pages parentes. On peut
le remplacer par un menu configuré par nos soins, et ajouter d'autres
menus à d'autres endroits du site.

**Apparence > Menus**

*Démo : créer le menu principal*

Renseigner le nom du menu, par exemple menu principal,
puis cliquer sur "créer le menu".

Les éléments à gauche permettent d'ajouter des contenus dans le menu.
On peut ajouter des pages, des articles, des catégories ou
des liens personnalisés.

À droite on gère l'organisation du menu lui-même, et la hiérarchie des
entrées de menus les unes par rapport aux autres.

Pour chaque entrée de menu, on peut modifier le nom affiché.

### Emplacements des menus

Pour afficher le menu dans la partie visible, il faut spécifier à quel
endroit on souhaite placer le menu. Le choix est dépendant du thème activé.

Démonstration : Faire une démo des éléments en lien avec les menus.

*Démo 2: créer d'autres menus*
- pour les réseaux sociaux
- pour le mobile
- pied de page


Conclusion : nous avons créé des menus, qui peuvent
des entrées vers des pages, des articles, des catégories ou des liens
(internes ou externes). 
On peut avoir des sous-menus, modifier le titre des entrées de menus. 
On peut placer des menus à différents endroit de la page selon le thème activé.

## Intégrer des Widgets

Chaque bloc situé à droite ou en pied de page est un widget.

Chaque widget est un "conteneur de contenus indépendants". Ils
permettent de mettre en avant des informations spécifiques et/ou des
options à destination des visiteurs du site.

Exemples : 
- un bloc de recherche
- articles récents
- commentaires récents
- archives
- catégories
- méta

**Apparence > Widgets**

Sur le thème twentytwenty seuls deux emplacements sont disponibles

- Pied de page #1
- Pied de page #2

Comme nous pour les menus, l'emplacement des widgets dépend
 du thème activé.

*Démo : ajouter des widgets en pied de page*

- une vidéo youtube
- une liste des articles récents
- un calendrier

Il existe un certain nombre de wigdets disponibles par défaut dans WP.
La liste des widgets disponibles dépend du thème activé
mais aussi des plugins installés.

## Ajouter un thème à son site

**Apparence > Thèmes**

Il y a 3 thèmes par défaut, le nom
de chaque thème correspond à l'année de sortie du thème.

Remarque : il y a une différence entre un thème installé et un thème
activé.

*Démonstration : activer un autre thème et l'activer*

Constater que les widgets et leurs emplacements ont changé avec le thème,
ainsi que pour les emplacements de menus.

En principe tous les thèmes WP sont Responsive.

*Démontration : Réduire la taille des fenêtres pour voir les effets.*

Les thèmes ne changent pas le contenu, mais le look du site.

### Personaliser un thème

Il est possible de personnaliser la présentation du thème activé.

*Démo : Personnaliser un thème*

- ajouter une icone au site web
- modifier les couleurs
- de placer une images d'en-tête
- une image d'arrière plan
- Les menus (identique à la section menus du tableau de bord)
- Les widgets (idem)
- choisir le type de la page d'accueil (page
spécifique plûtot que la liste des articles)

### Ajouter un nouveau thème

Il est possible d'ajouter des nouveaux thèmes. 

**Apparence > Thèmes → cliquer sur Ajouter un nouveau**

Bibliothèque de thèmes très fournie, fonctions de filtrage des thèmes.

*Démo : visualiser les thèmes, les filtrer selon quelques critères*

Remarque : Plus un thème est populaire, plus il est maintenu et facile à
mettre en oeuvre.

Il est conseillé de naviguer pour trouver son thème, qui dépend du type
de contenu. Il faut faire des essais...

*Démo : ajouter le thème Sparkling*

### Éditeur de thème

Il est possible de modifier les fichiers du thème, mais ce n'est pas conseillé par ce biais.
Il vaut mieux prévoir un thème-enfant pour pouvoir le faire, ainsi les modifications
ne seront pas écrasées à la mise à jour suivante.

On pourra voir ça dans une autre session, mais c'est plus du développement.
