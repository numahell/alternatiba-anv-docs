# Utilisation et installation Mumble

## Contexte et utilisation générale

-   Ce serveur Mumble est à disposition de l'ensemble du réseau
    Alternatiba / ANV-COP21 pour mener des réunions en interorga. Il a
    été mis en place pour proposer une solution face au confinement dû à
    la pandémie de coronavirus qui sévit actuellement.
-   Pour faciliter l'utilisation de l'outil, merci d'inscrire à l'avance
    vos réunions [dans ce
    planning](https://docs.google.com/spreadsheets/d/1RcsXtnNrWdXSHTVfoSO_RBKyDC61PC3s0QzBKT_XyJI/edit%23gid%3D802961176&sa=D&ust=1585308112486000) pour
    éviter que plusieurs réunions soient programmées simultanément dans
    le même salon de discussion ! Attention à bien lire la notice
    explicative dans le premier onglet avant de remplir le planning.
-   Cet outil n'est pas approprié pour des discussions nécessitant un
    haut niveau de confidentialité : n'importe qui connaissant les
    paramètres adresse et port indiqués plus bas peut accéder aux salons
    de discussion à tout moment ; chacun-e est libre de choisir un nom
    d'utilisateur, sans inscription ou vérification.

## Téléchargement

-   [Lien pour le télécharger le logiciel
    ​:](http://www.mumble.com/mumble-download.php&sa=D&ust=1585308112487000)[ ](http://www.mumble.com/mumble-download.php&sa=D&ust=1585308112488000)[https://www.mumble.com/](https://www.mumble.com/&sa=D&ust=1585308112488000) puis
    cliquez en haut à droite sur "Download Mumble". Vous pouvez aussi
    prendre l\'application Android
    ([Plumble](https://play.google.com/store/apps/details?id%3Dcom.morlunk.mumbleclient%26hl%3Den_US&sa=D&ust=1585308112488000)),
    ou iOS
    ([Mumble](https://itunes.apple.com/us/app/mumble/id443472808&sa=D&ust=1585308112488000))

## Installation

-   Ouvrez le fichier d'installation et aux questions qui vous sont
    posées lors de l'installation, répondre Oui ou OK à chaque fois
    (pour une configuration optimale de Mumble).

## Paramétrages initiaux

Un fois installé, ouvrez le logiciel. Une fenêtre apparaît. Ajouter un nouveau serveur :


-   ​Nom :  Interorga
-   adresse : reu-m.alternatiba.eu
-   port : 10091
-   nom d\'utilisateur : \"Prénom(Collectif-Ville)\" Attention, vous ne
    devez pas mettre d'espace dans votre nom d'utilisateur sinon ça ne
    fonctionne pas

Se connecter.

-   La première fois un message de certificat non valide apparaîtra,
    cliquez simplement sur Oui et la connection s\'établira.
-   Effectuer l\'assistant audio dans les paramètres (très important
    sinon vous n'arriverez pas à parler dans les réunions) :

- Au deuxième écran, décochez la case Activer l'audio positionnel

- Au cinquième écran, choisissez

- soit "Appuyer pour parler" et dans le champ correspondant, configurez
une touche qui sera utilisée pour activer la fonction. (vous devrez
appuyer sur une touche pour parler pendant les réunions)

- soit "Continu" (vous devrez décocher le micro pour parler et le
recocher quand vous aurez fini)

- Acceptez tous les autres paramètres par défaut et cliquez Suivant
jusqu'à la fin et cliquez sur Terminer

-   L'écran suivant vous permet à l'aide d'un assistant de générer un
    certificat qui est requis pour la connexion. Cliquez Suivant et
    ensuite Terminer

-   Désactiver la synthèse vocale en cliquant sur Configurer/synthèse
    vocale.

### Changements possibles après paramétrages initiaux

-   Si vous voulez changer votre pseudo alors que vous avez déjà
    installé et paramétré le logiciel, il suffit de cliquer sur éditer
    au lieu de connexion quand vous ouvrez le logiciel et changez votre
    pseudo
-   Si une voix répète tout ce qui est écrit dans le chat, désactiver la
    synthèse vocale en cliquant sur Configurer/synthèse vocale
-   Si vous entendez une notification sonore/un bip, à chaque nouveau
    message dans le chat, allez dans Configurer/Réglages/Messages (si
    vous ne voyez pas de section "Messages", il faut d'abord cocher la
    case "avancé" en bas à gauche de la fenêtre "Configurer/Réglages").
    Dans le grand tableau avec plein de cases à cocher, allez presque
    jusqu'en bas, sur la ligne "Message texte" et décocher la case dans
    la colonne "Fichier son" (voire toutes les cases de cette colonne).

### Paramètres spécifiques selon le système d'exploitation

Manipulations à suivre pour configurer sa touche pour parler sur
    Mumble :

-   sur Linux / Windows : menu Configurer =\> Réglages / Paramètres =\>
    si nécessaire, cocher la case Avancé en bas à droite =\> dans
    l'"onglet" Entrée audio (colonne d'onglets à gauche de la fenêtre),
    section Transmission, sélectionner Appuyer-pour-parler =\> dans
    l'"onglet" Raccourci, ajouter un raccourci pour Appuyer-pour-parler
    (on peut conseiller par exemple, la touche "pause" présents sur la
    plupart des claviers mais sans effet) ;
-   sur Mac : Mumble =\> Préférences =\> Raccourcis =\> Ajouter (une
    nouvelle ligne s'ajoute sur le tableau) =\> Cliquer sur "Non
    assigné" dans la colonne "Fonction" et choisir "cliquer pour parler"
    =\> Cliquer enfin sur la même ligne au niveau de la colonne
    "Raccourci" et choisir la touche pour parler (nous recommandons la
    touche "Commande" cmd).

## Signes écrits pour l'utilisation du chat

- ++   je lève les mains, je suis d\'accord
- \--  je baisse les mains, je ne suis pas d\'accord
- +- je suis plus ou moins d\'accord, à affiner/compléter
- \* ou parole je lève la main, je demande la parole
- \>\> La personne qui parle se répète, répète des choses déjà dites, parle trop longuement. Il faut avancer
- // le son coupe, je n'entends rien ou pas bien

## Autre tutoriels

- [http://revenudebase.info/mumble/](http://revenudebase.info/mumble/)
- [Tutoriel - installer et configurer mumble - fr - windows 7, 8, 10](https://www.youtube.com/watch?v%3DQR0BRlbAP_o)
